#!/bin/bash
set -e
umask 0002

. /usr/local/bin/setenv.sh

if [[ ! -e /usr/local/etc/icinga2/features-available/api.conf ]]; then
echo -e "\e[31mRun api setup\e[0m"
icinga2 api setup

  # Setup api-users.conf
  if [[ ! -e /usr/local/etc/icinga2/conf.d/api-users.conf ]]; then
    echo -e "\e[31mConfigure api-users.conf\e[0m"
    cat <<EOF > /usr/local/etc/icinga2/conf.d/api-users.conf
/**
 * The APIUser objects are used for authentication against the API.
 */
object ApiUser "$ICINGA_API_USERNAME" {
  password = "$ICINGA_API_PASSWORD"
  permissions = [ "*" ]
}
EOF
  fi
fi

cat <<EOF > /usr/local/etc/icinga2/features-available/ido-pgsql.conf
object IdoPgsqlConnection "ido-pgsql" {
  user = "$IDO_DB_USER"
  password = "$IDO_DB_PASS"
  host = "$IDO_DB_HOST"
  port = "$IDO_DB_PORT"
  database = "$IDO_DB"
}
EOF
