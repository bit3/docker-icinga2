#!/usr/bin/env python3
import sys, getopt, time, psycopg2

"""Check Database Utility


usage: db.py [options] [action]

action: is-up
        await-up
        database-exists
        table-exists [table_name]

general options:  -h [host] | --host [host]
                  -P [port] | --port [port]
                  -u [user] | --user [user]
                  -p [password] | --password [password]
                  -d [dbname] | --database [dbname]
         
await-up options: -t [seconds] | --ttl [seconds]
"""


def usage():
    """usage() -> None

    Shows usage informations.
    """

    print("""usage: db.py [options] [action]

action: is-up
        await-up
        database-exists
        table-exists [table_name]

general options:  -h [host] | --host [host]
                  -P [port] | --port [port]
                  -u [user] | --user [user]
                  -p [password] | --password [password]
                  -d [dbname] | --database [dbname]
         
await-up options: -t [seconds] | --ttl [seconds]""", file=sys.stderr)


def opts(argv):
    """opts(argv) -> database_host, database_port, database_user, database_pass, database_name, ttl, action

    Parses the program arguments and return the options.
    """

    database_host = None
    database_port = None
    database_user = None
    database_pass = None
    database_name = None
    ttl = 60

    try:
        opts, args = getopt.getopt(argv,"h:P:u:p:d:t:",["host=","port=","user=","password=","database=","ttl="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--host'):
            database_host = arg
        elif opt in ["-P", '--port']:
            database_port = arg
        elif opt in ("-u", "--user"):
            database_user = arg
        elif opt in ("-p", "--password"):
            database_pass = arg
        elif opt in ("-d", "--database"):
            database_name = arg
        elif opt in ("-t", "--ttl"):
            ttl = int(arg)

    if len(args) < 1:
        print('Error: exactly one action is expected', file=sys.stderr)
        usage()
        sys.exit(1)

    return database_host, database_port, database_user, database_pass, database_name, ttl, args[0], args[1:]


def connect(host, port, user, password, dbname):
    """connect(host, port, user, password, dbname) -> connection

    Connect to the database and return the connection.
    """

    return psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port)


def is_up(database_host, database_port, database_user, database_pass, database_name):
    """is_up(database_host, database_port, database_user, database_pass, database_name) -> bool

    Test if the database is up and respond to queries.
    """

    connection = None
    try:
        connection = connect(database_host, database_port, database_user, database_pass, database_name)
        cursor = connection.cursor()
        cursor.execute("SELECT 1")
        result = cursor.fetchone()
        cursor.close()

        return result is not None and result[0] == 1, None

    except (Exception, psycopg2.DatabaseError) as error:
        return False, error

    finally:
        if connection is not None:
            connection.close()


def await_up(database_host, database_port, database_user, database_pass, database_name, ttl):
    """await_up(database_host, database_port, database_user, database_pass, database_name, ttl) -> bool

    Await for the database to come up.
    """

    state, last_error = is_up(database_host, database_port, database_user, database_pass, database_name)
    if state:
        return True, None

    while ttl > 0:
        ttl -= 1
        print('.', end='', flush=True)
        time.sleep(1)

        state, last_error = is_up(database_host, database_port, database_user, database_pass, database_name)
        if state:
            print('')
            return True, None

    print('')
    return False, last_error


def database_exists(database_host, database_port, database_user, database_pass, database_name):
    """database_exists(database_host, database_port, database_user, database_pass, database_name) -> bool

    Test if the database exists.
    """

    connection = None
    try:
        connection = connect(database_host, database_port, database_user, database_pass, database_name)
        cursor = connection.cursor()
        cursor.execute("SELECT datname FROM pg_database WHERE datname = %(name)s", { 'name': database_name })
        result = cursor.fetchone()
        cursor.close()

        print(result)
        return result is not None and result[0] == database_name, None

    except (Exception, psycopg2.DatabaseError) as error:
        return False, error

    finally:
        if connection is not None:
            connection.close()


def table_exists(database_host, database_port, database_user, database_pass, database_name, table_name):
    """table_exists(database_host, database_port, database_user, database_pass, database_name) -> bool

    Test if the table exists.
    """

    connection = None
    try:
        connection = connect(database_host, database_port, database_user, database_pass, database_name)
        cursor = connection.cursor()
        cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_name = %(name)s", { 'name': table_name })
        result = cursor.fetchone()
        cursor.close()

        print(result)
        return result is not None and result[0] == table_name, None

    except (Exception, psycopg2.DatabaseError) as error:
        return False, error

    finally:
        if connection is not None:
            connection.close()


def die(message, last_error = None):
    print(message, file=sys.stderr)

    if last_error is not None:
        print("Last error:", last_error, file=sys.stderr)

    sys.exit(1)


def main(argv):
    database_host, database_port, database_user, database_pass, database_name, ttl, action, arguments = opts(argv)

    if 'is-up' == action:
        print('Check if database server {0}@{1}:{2} is up'.format(database_user, database_host, database_port))
        state, last_error = is_up(database_host, database_port, database_user, database_pass, database_name)
        if not state:
            die("Database server unavailable", last_error)

    elif 'await-up' == action:
        print('Await database server {0}@{1}:{2} is up'.format(database_user, database_host, database_port))
        state, last_error = await_up(database_host, database_port, database_user, database_pass, database_name, ttl)
        if not state:
            die("Database server unavailable", last_error)

    elif 'database-exists' == action:
        print('Check if database {0}@{1}:{2}/{3} exists'.format(database_user, database_host, database_port, database_name))
        state, last_error = database_exists(database_host, database_port, database_user, database_pass, database_name)
        if not state:
            die("Database does not exists", last_error)

    elif 'table-exists' == action:
        if len(arguments) < 1:
            die("Table name missing")
        elif len(arguments) > 1:
            die("Exactly one table name expected")

        table_name = arguments[0]

        print('Check if table {0}@{1}:{2}/{3}/{4} exists'.format(database_user, database_host, database_port, database_name, table_name))
        state, last_error = table_exists(database_host, database_port, database_user, database_pass, database_name, table_name)
        if not state:
            die("Table does not exists", last_error)

    else:
        print('Invalid argument: {0}'.format(action,), file=sys.stderr)
        usage()
        sys.exit(1)


if __name__ == '__main__':
    main(sys.argv[1:])
