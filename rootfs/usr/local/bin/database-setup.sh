#!/usr/bin/env bash
set -ex
umask 0002

. /usr/local/bin/setenv.sh

function ido_db_psql {
  PGPASSWORD="$IDO_DB_PASS" psql -h "$IDO_DB_HOST" -p "$IDO_DB_PORT" -U "$IDO_DB_USER" "$IDO_DB" "$@"
  return $?
}

function ido_db_check {
  db-check.py -h "$IDO_DB_HOST" -P "$IDO_DB_PORT" -u "$IDO_DB_USER" -p "$IDO_DB_PASS" -d "$IDO_DB" "$@"
}

ido_db_check "await-up"

ido_db_check "database-exists" || (
  echo -e "\e[31mDatabase $IDO_DB does not exists, aborting...\e[0m"
  exit 1
)

ido_db_check "table-exists" "icinga_dbversion" || (
  echo -e "\e[31mInitial import database\e[0m"
  ido_db_psql < /usr/local/share/icinga2-ido-pgsql/schema/pgsql.sql
)

# TODO Database upgrade path
