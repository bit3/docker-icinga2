#!/bin/bash
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
  set -- icinga2 "$@"
fi

if [ "$1" = "icinga2" ]; then

  if [[ -x /usr/local/bin/database-setup.sh ]]; then
    /usr/local/bin/database-setup.sh
  fi

  if [[ -x /usr/local/bin/icinga-setup.sh ]]; then
    /usr/local/bin/icinga-setup.sh
  fi

  if [[ -x /usr/local/bin/ssmtp-setup.sh ]]; then
    /usr/local/bin/ssmtp-setup.sh
  fi

fi

exec "$@"
