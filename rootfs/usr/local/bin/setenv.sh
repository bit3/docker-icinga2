#!/usr/bin/env bash

function read_file_or_value {
  if [[ -n "$1" ]]; then
    echo $(cat "$1")
  else
    echo "$2"
  fi
}

export IDO_DB_HOST=$(read_file_or_value "${IDO_DB_HOST_FILE}" "${IDO_DB_HOST}")
export IDO_DB_PORT=$(read_file_or_value "${IDO_DB_PORT_FILE}" "${IDO_DB_PORT}")
export IDO_DB_USER=$(read_file_or_value "${IDO_DB_USER_FILE}" "${IDO_DB_USER}")
export IDO_DB_PASS=$(read_file_or_value "${IDO_DB_PASS_FILE}" "${IDO_DB_PASS}")
export IDO_DB=$(read_file_or_value "${IDO_DB_FILE}" "${IDO_DB}")

export ICINGA_API_USERNAME=$(read_file_or_value "${ICINGA_API_USERNAME_FILE}" "${ICINGA_API_USERNAME}")
export ICINGA_API_PASSWORD=$(read_file_or_value "${ICINGA_API_PASSWORD_FILE}" "${ICINGA_API_PASSWORD}")

export MAIL_HUB=$(read_file_or_value "${MAIL_HUB_FILE}" "${MAIL_HUB}")
export MAIL_ROOT=$(read_file_or_value "${MAIL_ROOT_FILE}" "${MAIL_ROOT}")
export MAIL_REWRITE_DOMAIN=$(read_file_or_value "${MAIL_REWRITE_DOMAIN_FILE}" "${MAIL_REWRITE_DOMAIN}")
export MAIL_HOSTNAME=$(read_file_or_value "${MAIL_HOSTNAME_FILE}" "${MAIL_HOSTNAME}")
export MAIL_USERNAME=$(read_file_or_value "${MAIL_USERNAME_FILE}" "${MAIL_USERNAME}")
export MAIL_PASSWORD=$(read_file_or_value "${MAIL_PASSWORD_FILE}" "${MAIL_PASSWORD}")
