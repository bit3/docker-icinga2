#!/bin/bash
set -e

. /usr/local/bin/setenv.sh

cat <<EOF > /etc/ssmtp/ssmtp.conf
root=$MAIL_ROOT
mailhub=$MAIL_HUB
rewriteDomain=$MAIL_REWRITE_DOMAIN
hostname=$MAIL_HOSTNAME
FromLineOverride=NO

UseTLS=YES
UseSTARTTLS=YES

AuthMethod=LOGIN
AuthUser=$MAIL_USERNAME
AuthPass=$MAIL_PASSWORD
EOF
