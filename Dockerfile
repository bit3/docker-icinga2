FROM ubuntu:18.04

ENV ICINGA_VERSION="2.9.1" \
    NAGIOS_PLUGINS_VERSION="2.2.1" \
    ICINGA_API_USERNAME="root" \
    ICINGA_API_PASSWORD="super-secret" \
    IDO_DB_HOST="postgres" \
    IDO_DB_PORT="5432" \
    IDO_DB_USER="postgres" \
    IDO_DB_PASS="postgres" \
    IDO_DB="icinga" \
    MAIL_HUB="mail.example.com" \
    MAIL_ROOT="admin@example.com" \
    MAIL_REWRITE_DOMAIN="" \
    MAIL_HOSTNAME="icinga.example.com" \
    MAIL_USERNAME="icinga" \
    MAIL_PASSWORD="super-secret"

RUN set -xe; \
    export DEBIAN_FRONTEND=noninteractive; \
    mkdir mkdir -p /usr/share/man/man1 /usr/share/man/man7; \
    apt update; \
    apt install -y \
      # build packages, can be removed after build
      cmake \
      make \
      build-essential \
      pkg-config \
      libssl-dev \
      libboost-all-dev \
      bison \
      flex \
      libsystemd-dev \
      libpq-dev \
      libedit-dev \
      wget \
      # runtime packages
      libboost-program-options1.65.1 \
      libboost-regex1.65.1 \
      libboost-thread1.65.1 \
      libedit2 \
      libpq5 \
      postgresql-client \
      python3-psycopg2 \
      mailutils \
      ssmtp \
      # https://github.com/monitoring-plugins/monitoring-plugins/issues/1509#issuecomment-341444373
      iputils-ping; \
    # Download sources
    echo "Fetch Icinga2 ${ICINGA_VERSION}"; \
    mkdir /usr/src/icinga2; \
    wget -q -O - https://github.com/Icinga/icinga2/archive/v${ICINGA_VERSION}.tar.gz \
      | tar xz --strip 1 -C /usr/src/icinga2; \
    echo "Fetch Nagios Plugins ${NAGIOS_PLUGINS_VERSION}"; \
    mkdir /usr/src/nagios-plugins; \
    wget -q -O - https://nagios-plugins.org/download/nagios-plugins-${NAGIOS_PLUGINS_VERSION}.tar.gz \
      | tar xz --strip 1 -C /usr/src/nagios-plugins; \
    # Prepare environment
    groupadd --system icinga; \
    groupadd --system icingacmd; \
    useradd --system -c "icinga" -s /sbin/nologin -G icingacmd -g icinga icinga; \
    # Build icinga
    cd /usr/src/icinga2; \
    mkdir release && cd release; \
    cmake .. \
      -DICINGA2_GIT_VERSION_INFO=OFF \
      -DUSE_SYSTEMD=ON \
      -DICINGA2_WITH_MYSQL=OFF \
      -DICINGA2_WITH_PGSQL=ON \
      -DICINGA2_PLUGINDIR=/usr/local/nagios/libexec; \
    cd ..; \
    make -C release; \
    make install -C release; \
    # Post build steps
    chown -R icinga:icinga /usr/local/var/cache/icinga2; \
    chown -R icinga:icinga /usr/local/var/lib/icinga2; \
    chown -R icinga:icinga /usr/local/var/log/icinga2; \
    chown -R icinga:icinga /usr/local/var/run/icinga2; \
    icinga2 feature enable ido-pgsql; \
    # Build nagios plugins
    cd /usr/src/nagios-plugins; \
    ./configure; \
    make; \
    make install; \
    # Delete sources
    rm -rf /usr/src/icinga2 /usr/src/nagios-plugins; \
    # remove build packages
    apt remove --purge -y \
      cmake \
      make \
      build-essential \
      pkg-config \
      libssl-dev \
      libboost-all-dev \
      bison \
      flex \
      libsystemd-dev \
      libpq-dev \
      libedit-dev \
      wget; \
    apt autoremove --purge -y; \
    rm -rf /var/lib/apt/lists/*;

ADD rootfs /

EXPOSE 5665

VOLUME ["/usr/local/etc/icinga2"]
VOLUME ["/usr/local/var/cache/icinga2"]
VOLUME ["/usr/local/var/lib/icinga2"]
VOLUME ["/usr/local/var/log/icinga2"]

#HEALTHCHECK CMD nc -z localhost 9000

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["icinga2", "daemon"]
